'use strict'
const  mongoose = require('mongoose');
const  schema = mongoose.Schema;

const drugSchema = new schema({

    did:String,
    category:String,
    name:String,
    type:String,
    price:Number
});

const drug =mongoose.model('drug',drugSchema);
module.exports =drug;
