'use strict';

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.Promise = global.Promise;

require('./models/Store_Db_Models/categoryDbModel');
require('./models/Store_Db_Models/drugDbModel');

const cateRouter = require('./routes/Store-Routes/categoryRoutes');
const drugRouter = require('./routes/Store-Routes/drugRoute');

const app = express();

app.use(bodyParser.json());
app.use('/', express.static(__dirname + '/public'));

mongoose.connect('mongodb://127.0.0.1:27017/sampleptest', err => {
    if (err) {
        console.log(err);
        process.exit(1);
    }
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});
app.use('/store/category',cateRouter);
app.use('/store/drug',drugRouter);


app.listen(3000, err => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('app listening on port 3000');
});
